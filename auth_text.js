const https = require('https');
const querystring = require('querystring');
const url = require('url');

const clientSecret = 'doM63QWJe7NUiXlo2R8X7nHnI8uqyVwD';
const clientId = 'v4zS40iO9GRGgfK7hdRfbjSHd4hZrlB7';
const myToken = 'v1K1VRpAuGwy4ZZPlCw1CBtUYPZxkxHVkdujCKQogjyhw';
var token = '';
var code = 'ea3GGtG2puebYtISX3i9YmWjXnLlbKSU';

var data = querystring.stringify({
    'client_id': clientId,
    'client_secret': clientSecret,
    'code': code,
    'grant_type': 'authorization_code'
});

var options = {
    hostname: 'api.pushbullet.com',
    path: '/oauth2/token',
    method: 'POST',
    rejectUnauthorized: false,
    headers: {
        'Access-Token': myToken,
        'Content-Type': 'application/json'
    }
};

var temp_token = https.request(options, function(res) {
    console.log('STATUS: ' + res.statusCode);
    console.log('HEADERS: ' + JSON.stringify(res.headers));
    res.setEncoding('utf8');

    console.log(res);
    res.on('data', function(blob) {
        console.log('BODY: ' + blob);
    });

    res.on('end', function() {
        console.log('No more data in response.');
    })
});

temp_token.on('error', function(error) {
    console.log('ERROR: ' + error.message);
});

temp_token.write('');
temp_token.end();

/**
curl --header 'Access-Token: v1K1VRpAuGwy4ZZPlCw1CBtUYPZxkxHVkdujCKQogjyhw' \
     --header 'Content-Type: application/json' \
     --data-binary '{"client_id":"v4zS40iO9GRGgfK7hdRfbjSHd4hZrlB7","client_secret":"doM63QWJe7NUiXlo2R8X7nHnI8uqyVwD","code":"ea3GGtG2puebYtISX3i9YmWjXnLlbKSU","grant_type":"authorization_code"}' \
     --request POST \
     https://api.pushbullet.com/oauth2/token
*/
