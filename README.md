# Fram
From the [ancient language](http://inheritance.wikia.com/wiki/Ancient_Language) meaning *forward*

A pushbullet client for all, powered by [Electron](https://www.electron.github.io) from [Github](https://www.github.com).

