const bodyParser = require('body-parser');
const bs = require('browser-sync');
const compression = require('compression');
const express = require('express');
const https = require('https');
const jade = require('jade');
const morgan = require('morgan');
const stylus = require('stylus');
const url = require('url');

const app = express();
const env = process.argv[2] || 'prod';
const port = (env == 'dev') ? 2368 : 3000;
const viewPath = (env == 'dev') ? '/app/src/jade' : '/app/dist/views';

const clientSecret = 'doM63QWJe7NUiXlo2R8X7nHnI8uqyVwD';
const clientId = 'v4zS40iO9GRGgfK7hdRfbjSHd4hZrlB7';
const myToken = 'v1K1VRpAuGwy4ZZPlCw1CBtUYPZxkxHVkdujCKQogjyhw';
var token = '';
var isAuthenticated = false;


function compile(str, path) {
    return stylus(str)
            .set('filename', path)
            .set('compress', true)
}

app.set('views', __dirname + viewPath);
app.set('view engine', 'jade');
app.use(morgan('combined'));
app.use(stylus.middleware({
        src: __dirname + '/app/src/stylus',
        dest: __dirname + '/app/dist/css',
        compress: true,
        compile: compile
    })
);

app.use(express.static(__dirname + '/app/dist'));

app.get('/auth_yay', function(req, res) {
    isAuthenticated = !isAuthenticated;
    var code = url.parse(req.url, true).query.code;
    var options = {
        hostname: 'api.pushbullet.com'
        path: '/oauth/token',
        method: 'POST',
        headers: {
            'Access-Token': myToken,
            'Content-Type': 'application/json'
        }
    }
    var data = {
        'client_id': clientId,
        'client_secret': clientSecret,
        'code': code,
        'grant_type': 'authorization_code'
    };

    var temp_token = https.requests(options, function(res) {
        console.log('STATUS: ' + res.statusCode);
        console.log('HEADERS: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');

        res.on('data', function(blob) {
            console.dir('BODY: ' + blob);
        });
    });

    temp_token.on('error', function(error) {
        console.log('ERROR: ' + error.message);
    });

    temp_token.write(data);
    temp_token.end();

    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('<h1>Auth Yay!</h1>');
    res.write('<p> token: ' + token + ' </p>');
    res.end();
});

app.get('*', function(req, res) {
    res.render('index', {page: 'Home'});
});

app.listen(port, function() {
    if (env == 'dev') {
        bs({
            files: ['app/src/*/*.{jade,styl}'],
            open: false,
            proxy: 'localhost:' + port
        });
        console.log('Listening on port 2368 w/ browser-sync');
    } else {
        console.log('Listening on port ' + port);
    }
});
